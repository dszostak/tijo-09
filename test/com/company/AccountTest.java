package com.company;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AccountTest {

    private BankOperation bank;

    @Before
    public void initDataForAccount() {

        bank = new Bank();
    }

    @Test
    public void checkCorrectOperationForAccount() {

        /* test create bank */
        Assert.assertEquals("Zalozenie konta o numerze: 1", 1, bank.createAccount());
        Assert.assertEquals("Zalozenie konta o numerze: 2", 2, bank.createAccount());
        Assert.assertEquals("Zalozenie konta o numerze: 3", 3, bank.createAccount());
        Assert.assertEquals("Zalozenie konta o numerze: 4", 4, bank.createAccount());

        /* test delete bank */
        Assert.assertEquals("Likwidacja konta nr. 2", 0, bank.deleteAccount(2));
        Assert.assertEquals("Likwidacja konta nr. 5", -1, bank.deleteAccount(5 ));

        /* test create bank */
        Assert.assertEquals("Zalozenie konta o numerze: 5", 5, bank.createAccount());

        /* test deposit amount */
        Assert.assertTrue("Wplacam 100 zl na nr. 1", bank.deposit( 1, 100));
        Assert.assertFalse("Wplacam 200 zl na nr. 2", bank.deposit( 2, 200));
        Assert.assertTrue("Wplacam 300 zl na nr. 3", bank.deposit( 3, 300));
        Assert.assertTrue("Wplacam 400 zl na nr. 4", bank.deposit( 4, 400));
        Assert.assertTrue("Wplacam 500 zl na nr. 5", bank.deposit( 5, 500));
        Assert.assertTrue("Wplacam 450 zl na nr. 5", bank.deposit( 5, 450));

        Assert.assertFalse("Wplacam -5 zl na nr. 1", bank.deposit( 4, -5));

        /* test bank balance */
        Assert.assertEquals("Stan konta nr. 1", 100, bank.accountBalance(1));
        Assert.assertEquals("Stan konta nr. 3", 300, bank.accountBalance(3));
        Assert.assertEquals("Stan konta nr. 5", 950, bank.accountBalance(5));

        /* test withdraw amount from bank */
        Assert.assertTrue("Wyplacam 50 zl z nr. 1", bank.withdraw( 1, 50));
        Assert.assertTrue("Wyplacam 75 zl z nr. 3", bank.withdraw( 3, 75));
        Assert.assertFalse("Wyplacam 1000 zl z nr. 5", bank.withdraw( 5, 1000));

        /* test bank balance */
        Assert.assertEquals("Stan konta nr. 1", 50, bank.accountBalance(1));
        Assert.assertEquals("Stan konta nr. 3", 225, bank.accountBalance(3));
        Assert.assertEquals("Stan konta nr. 5", 950, bank.accountBalance(5));

        /* test bank transfer - 1 - */
        Assert.assertTrue("Przelew z konta nr. 3 na konto nr. 5 (25 zl)",
                bank.transfer(3, 5, 25));

        /* test transaction result */
        Assert.assertEquals("Stan konta nr. 3", 200, bank.accountBalance(3));
        Assert.assertEquals("Stan konta nr. 5", 975, bank.accountBalance(5));

        /* test bank transfer - 2 - */
        Assert.assertFalse("Przelew z konta nr. 3 na konto nr. 5 (210 zl)",
                bank.transfer(3, 5, 210));

        /* test transaction result */
        Assert.assertEquals("Stan konta nr. 3", 200, bank.accountBalance(3));
        Assert.assertEquals("Stan konta nr. 5", 975, bank.accountBalance(5));

        /* test bank transfer - 3 - */
        Assert.assertFalse("Przelew z konta nr. 3 na konto nr. 5 (200 zl)",
                bank.transfer(3, -1, 200));

        /* test transaction result */
        Assert.assertEquals("Stan konta nr. 3", 200, bank.accountBalance(3));
        Assert.assertEquals("Stan konta nr. 5", 975, bank.accountBalance(5));

        /* test bank balance */
        Assert.assertEquals("Stan konta nr. 1", 50, bank.accountBalance(1));
        Assert.assertEquals("Stan konta nr. 2", -1, bank.accountBalance(2));
        Assert.assertEquals("Stan konta nr. 3", 200, bank.accountBalance(3));
        Assert.assertEquals("Stan konta nr. 4", 400, bank.accountBalance(4));
        Assert.assertEquals("Stan konta nr. 5", 975, bank.accountBalance(5));
        Assert.assertEquals("Stan konta nr. 6", -1, bank.accountBalance(6));

        /* test create bank */
        Assert.assertEquals("Zalozenie konta o numerze: 6", 6, bank.createAccount());

        /* test bank balance */
        Assert.assertEquals("Stan konta nr. 6", 0, bank.accountBalance(6));


        /* test sum bank balance */
        Assert.assertEquals("Stan skarbca", 1625, bank.sumAccountsBalance());
    }
}
